import { createTransport } from "nodemailer"
import smtpTransport from 'nodemailer-smtp-transport';
import Mail from "nodemailer/lib/mailer";
import { readFileSync, readFile } from 'fs'
import { BadRequestError } from "../errors/bad-request-error"
/*Ref: 
https://stackoverflow.com/questions/19877246/nodemailer-with-gmail-and-nodejs

Step 1:
Go here https://myaccount.google.com/lesssecureapps and enable for less secure apps. If this does not work then

Step 2
Go here https://accounts.google.com/DisplayUnlockCaptcha and enable/continue and then try.
for me step 1 alone didn't work so i had to go to step 2.
i also tried removing the nodemailer-smtp-transport package and to my surprise it works. but then when i restarted my system it gave me same error, so i had to go and turn on the less secure app (i disabled it after my work).
then for fun i just tried it with off(less secure app) and vola it worked again!
*/


// var mailOptions = {
//   from: 'vivekmarve99@gmail.com',
//   to: 'kprasad1116@gmail.com',
//   subject: 'Sending Email using Node.js[nodemailer]',
//   text: 'That was easy!'
// };

export interface mailOptions {
  from: string;
  to: string | string[];
  subject: string;
  text?: string;
  html?: string;
};

export interface resetPasswordParams {
  preview: string;
  name: string;
  message: string;
  link: string;
}

export class SendMail {
  googleUsername: string = "vivekmarve99@gmail.com";
  googlePassword: string = "Cg.12345";
  mailOptions: mailOptions;
  transporter: Mail;
  constructor(mailOptions: mailOptions) {
    this.mailOptions = mailOptions;
    this.transporter = createTransport(smtpTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      auth: {
        user: this.googleUsername,
        pass: this.googlePassword
      }
    }));
  }

  set to(value: string) {
    if (typeof this.mailOptions.to == 'string') {
      this.mailOptions.to = [this.mailOptions.to, value];
    } else {
      this.mailOptions.to.push(value);
    }
  }

  set many(value: string[]) {
    if (typeof this.mailOptions.to == 'string') {
      this.mailOptions.to = [this.mailOptions.to, ...value];
    } else {
      this.mailOptions.to.push(...value);
    }
  }

  resetPassword(resetPasswordParams: resetPasswordParams | any,cb:(err: Error | null, info: any) => void) {
    let replacer = [
      { key: 'preview', target: "__PREVIEW__" },
      { key: 'name', target: "__NAME__" },
      { key: 'message', target: "__MESSAGE__" },
      { key: 'link', target: "__LINK__" },
    ]
    let template = readFileSync('./template/reset-password.html', 'utf8').toString();
    for (let i = 0; i < replacer.length; i++) {
      const el = replacer[i];
      if (resetPasswordParams.hasOwnProperty(el.key)) {
        template = template.replace(el.target, resetPasswordParams[el.key]);
      } else {
        template = template.replace(el.target, '');
      }
    }
    this.mailOptions.html = template;
    this.send(cb);
  }


  send = (mailSendCallback: (err: Error | null, info: any) => void) => {
    this.transporter.sendMail(this.mailOptions, mailSendCallback)
    // if(this.mailOptions.html || this.mailOptions.text){
    // }else{
    //   throw new BadRequestError("Please set html OR test in mailOptions");
    // }
    //   (error, info) => {
    //   if (error) {
    //     cb(error, null);
    //   } else {
    //     console.log('Email sent: ' + info.response);
    //     cb(null, info)
    //   }
    // });
  }
};

