import { Subjects } from './subjects';

export interface InventoryUpdatedEvent {
  subject: Subjects.InventoryUpdated;
  data: any
  // {
  //   id: string;
  //   title: string;
  //   price: number;
  //   userId: string;
  // };
}
