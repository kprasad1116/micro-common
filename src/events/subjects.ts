export enum Subjects {
  InventoryCreated = 'inventory:created',
  InventoryUpdated = 'inventory:updated',
}
