import { Subjects } from './subjects';

export interface InventoryCreatedEvent {
  subject: Subjects.InventoryCreated;
  data: any
  // {
  //   id: string;
  //   title: string;
  //   price: number;
  //   userId: string;
  // };
}
