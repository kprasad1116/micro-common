import { Request, Response, NextFunction } from 'express';
import { NotAuthorizedError } from '../errors/not-authorized-error';
import { extractjwt } from './current-user';

export const requireAuth = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let user = extractjwt(req);
  req.currentUser = user;
  console.log('user ==>', user)
  if (user && user.userType) {
    res.setHeader('userType', user.userType);
    res.set('userType', user.userType);
    res.header('userType', user.userType);
    console.log('header set ==>', user.userType)
  }
  if (!req.currentUser) {
    throw new NotAuthorizedError();
  }
  next();
};
