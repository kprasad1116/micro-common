import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

interface UserPayload {
  id: string;
  email: string;
  userType: string;
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload;
    }
  }
}

export const currentUser = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let loginkey = req.get('loginkey')?.toString();
  if (!req.session?.jwt || loginkey) {
    return next();
  }
  req.currentUser = extractjwt(req);
  next();
};

export const extractjwt = (req: Request): any => {
  try {
    let loginkey = req.get('loginkey')?.toString();
    console.log('loginkey => ', loginkey);
    console.log('req.session?.jwt => ', req.session?.jwt);
    const payload = jwt.verify(
      loginkey! || req.session?.jwt,
      process.env.JWT_KEY!
    ) as UserPayload;
    return payload;
  } catch (err) {
    return null;
  }
}
