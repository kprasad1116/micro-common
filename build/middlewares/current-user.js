"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractjwt = exports.currentUser = void 0;
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.currentUser = function (req, res, next) {
    var _a, _b;
    var loginkey = (_a = req.get('loginkey')) === null || _a === void 0 ? void 0 : _a.toString();
    if (!((_b = req.session) === null || _b === void 0 ? void 0 : _b.jwt) || loginkey) {
        return next();
    }
    req.currentUser = exports.extractjwt(req);
    next();
};
exports.extractjwt = function (req) {
    var _a, _b, _c;
    try {
        var loginkey = (_a = req.get('loginkey')) === null || _a === void 0 ? void 0 : _a.toString();
        console.log('loginkey => ', loginkey);
        console.log('req.session?.jwt => ', (_b = req.session) === null || _b === void 0 ? void 0 : _b.jwt);
        var payload = jsonwebtoken_1.default.verify(loginkey || ((_c = req.session) === null || _c === void 0 ? void 0 : _c.jwt), process.env.JWT_KEY);
        return payload;
    }
    catch (err) {
        return null;
    }
};
