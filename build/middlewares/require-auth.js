"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.requireAuth = void 0;
var not_authorized_error_1 = require("../errors/not-authorized-error");
var current_user_1 = require("./current-user");
exports.requireAuth = function (req, res, next) {
    var user = current_user_1.extractjwt(req);
    req.currentUser = user;
    if (user && user.userType) {
        res.setHeader('userType', user.userType);
        res.set('userType', user.userType);
        res.header('userType', user.userType);
    }
    if (!req.currentUser) {
        throw new not_authorized_error_1.NotAuthorizedError();
    }
    next();
};
