import { Request, Response, NextFunction } from 'express';
interface UserPayload {
    id: string;
    email: string;
    userType: string;
}
declare global {
    namespace Express {
        interface Request {
            currentUser?: UserPayload;
        }
    }
}
export declare const currentUser: (req: Request, res: Response, next: NextFunction) => void;
export declare const extractjwt: (req: Request) => any;
export {};
