import Mail from "nodemailer/lib/mailer";
export interface mailOptions {
    from: string;
    to: string | string[];
    subject: string;
    text?: string;
    html?: string;
}
export interface resetPasswordParams {
    preview: string;
    name: string;
    message: string;
    link: string;
}
export declare class SendMail {
    googleUsername: string;
    googlePassword: string;
    mailOptions: mailOptions;
    transporter: Mail;
    constructor(mailOptions: mailOptions);
    set to(value: string);
    set many(value: string[]);
    resetPassword(resetPasswordParams: resetPasswordParams | any, cb: (err: Error | null, info: any) => void): void;
    send: (mailSendCallback: (err: Error | null, info: any) => void) => void;
}
