"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendMail = void 0;
var nodemailer_1 = require("nodemailer");
var nodemailer_smtp_transport_1 = __importDefault(require("nodemailer-smtp-transport"));
var fs_1 = require("fs");
;
var SendMail = /** @class */ (function () {
    function SendMail(mailOptions) {
        var _this = this;
        this.googleUsername = "vivekmarve99@gmail.com";
        this.googlePassword = "Cg.12345";
        this.send = function (mailSendCallback) {
            _this.transporter.sendMail(_this.mailOptions, mailSendCallback);
            // if(this.mailOptions.html || this.mailOptions.text){
            // }else{
            //   throw new BadRequestError("Please set html OR test in mailOptions");
            // }
            //   (error, info) => {
            //   if (error) {
            //     cb(error, null);
            //   } else {
            //     console.log('Email sent: ' + info.response);
            //     cb(null, info)
            //   }
            // });
        };
        this.mailOptions = mailOptions;
        this.transporter = nodemailer_1.createTransport(nodemailer_smtp_transport_1.default({
            service: 'gmail',
            host: 'smtp.gmail.com',
            auth: {
                user: this.googleUsername,
                pass: this.googlePassword
            }
        }));
    }
    Object.defineProperty(SendMail.prototype, "to", {
        set: function (value) {
            if (typeof this.mailOptions.to == 'string') {
                this.mailOptions.to = [this.mailOptions.to, value];
            }
            else {
                this.mailOptions.to.push(value);
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SendMail.prototype, "many", {
        set: function (value) {
            var _a;
            if (typeof this.mailOptions.to == 'string') {
                this.mailOptions.to = __spreadArrays([this.mailOptions.to], value);
            }
            else {
                (_a = this.mailOptions.to).push.apply(_a, value);
            }
        },
        enumerable: false,
        configurable: true
    });
    SendMail.prototype.resetPassword = function (resetPasswordParams, cb) {
        var replacer = [
            { key: 'preview', target: "__PREVIEW__" },
            { key: 'name', target: "__NAME__" },
            { key: 'message', target: "__MESSAGE__" },
            { key: 'link', target: "__LINK__" },
        ];
        var template = fs_1.readFileSync('./template/reset-password.html', 'utf8').toString();
        for (var i = 0; i < replacer.length; i++) {
            var el = replacer[i];
            if (resetPasswordParams.hasOwnProperty(el.key)) {
                template = template.replace(el.target, resetPasswordParams[el.key]);
            }
            else {
                template = template.replace(el.target, '');
            }
        }
        this.mailOptions.html = template;
        this.send(cb);
    };
    return SendMail;
}());
exports.SendMail = SendMail;
;
