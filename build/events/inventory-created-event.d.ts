import { Subjects } from './subjects';
export interface InventoryCreatedEvent {
    subject: Subjects.InventoryCreated;
    data: any;
}
