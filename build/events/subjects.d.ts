export declare enum Subjects {
    InventoryCreated = "inventory:created",
    InventoryUpdated = "inventory:updated"
}
