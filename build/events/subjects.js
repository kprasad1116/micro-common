"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Subjects = void 0;
var Subjects;
(function (Subjects) {
    Subjects["InventoryCreated"] = "inventory:created";
    Subjects["InventoryUpdated"] = "inventory:updated";
})(Subjects = exports.Subjects || (exports.Subjects = {}));
