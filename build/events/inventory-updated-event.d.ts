import { Subjects } from './subjects';
export interface InventoryUpdatedEvent {
    subject: Subjects.InventoryUpdated;
    data: any;
}
